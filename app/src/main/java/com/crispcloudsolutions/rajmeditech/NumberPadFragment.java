package com.crispcloudsolutions.rajmeditech;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

public class NumberPadFragment extends Fragment implements View.OnClickListener{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    Button  number0,
            number1,
            number2,
            number3,
            number4,
            number5,
            number6,
            number7,
            number8,
            number9,
            title,
            menu,
            reverse,
            display,
            subtitle,
            language,
            start,
            hash,
            clear,
            pause,
            forward;

    private Socket socket;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_number_pad, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        number0=(Button)getActivity().findViewById(R.id.number0);
        number1=(Button)getActivity().findViewById(R.id.number1);
        number2=(Button)getActivity().findViewById(R.id.number2);
        number3=(Button)getActivity().findViewById(R.id.number3);
        number4=(Button)getActivity().findViewById(R.id.number4);
        number5=(Button)getActivity().findViewById(R.id.number5);
        number6=(Button)getActivity().findViewById(R.id.number6);
        number7=(Button)getActivity().findViewById(R.id.number7);
        number8=(Button)getActivity().findViewById(R.id.number8);
        number9=(Button)getActivity().findViewById(R.id.number9);
        title=(Button)getActivity().findViewById(R.id.title);
        menu=(Button)getActivity().findViewById(R.id.menu);
        reverse=(Button)getActivity().findViewById(R.id.reverse);
        display=(Button)getActivity().findViewById(R.id.display);
        subtitle=(Button)getActivity().findViewById(R.id.subtitle);
        language=(Button)getActivity().findViewById(R.id.language);
        start=(Button)getActivity().findViewById(R.id.start);
        hash=(Button)getActivity().findViewById(R.id.hash);
        clear=(Button)getActivity().findViewById(R.id.clear);
        pause=(Button)getActivity().findViewById(R.id.pause);
        forward=(Button)getActivity().findViewById(R.id.forward);

        try {
            socket = IO.socket("http://192.168.42.1:3000");
        }catch (Exception e){
            e.printStackTrace();
        }

        number0.setOnClickListener(this);
        number1.setOnClickListener(this);
        number2.setOnClickListener(this);
        number3.setOnClickListener(this);
        number4.setOnClickListener(this);
        number5.setOnClickListener(this);
        number6.setOnClickListener(this);
        number7.setOnClickListener(this);
        number8.setOnClickListener(this);
        number9.setOnClickListener(this);
        title.setOnClickListener(this);
        menu.setOnClickListener(this);
        reverse.setOnClickListener(this);
        display.setOnClickListener(this);
        subtitle.setOnClickListener(this);
        language.setOnClickListener(this);
        start.setOnClickListener(this);
        hash.setOnClickListener(this);
        clear.setOnClickListener(this);
        pause.setOnClickListener(this);
        forward.setOnClickListener(this);

        socket.connect();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.number0:
                socket.emit("mobile_request","KEY_MENU");
                break;
            case R.id.number1:
                socket.emit("mobile_request","KEY_RED");
                break;
            case R.id.number2:
                socket.emit("mobile_request","KEY_YELLOW");
                break;
            case R.id.number3:
                socket.emit("mobile_request","KEY_3");
                break;
            case R.id.number4:
                socket.emit("mobile_request","KEY_4");
                break;
            case R.id.number5:
                socket.emit("mobile_request","KEY_5");
                break;
            case R.id.number6:
                socket.emit("mobile_request","KEY_6");
                break;
            case R.id.number7:
                socket.emit("mobile_request","KEY_7");
                break;
            case R.id.number8:
                socket.emit("mobile_request","KEY_8");
                break;
            case R.id.number9:
                socket.emit("mobile_request","KEY_9");
                break;
            case R.id.title:
                socket.emit("mobile_request","KEY_E");
                break;
            case R.id.menu:
                socket.emit("mobile_request","KEY_0");
                break;
            case R.id.reverse:
                socket.emit("mobile_request","KEY_F1");
                break;
            case R.id.display:
                socket.emit("mobile_request","KEY_F2");
                break;
            case R.id.subtitle:
                socket.emit("mobile_request","KEY_F3");
                break;
            case R.id.language:
                socket.emit("mobile_request","KEY_F4");
                break;
            case R.id.start:
                socket.emit("mobile_request","KEY_F5");
                break;
            case R.id.hash:
                socket.emit("mobile_request","KEY_F6");
                break;
            case R.id.clear:
                socket.emit("mobile_request","KEY_F7");
                break;
            case R.id.pause:
                socket.emit("mobile_request","KEY_F8");
                break;
            case R.id.forward:
                socket.emit("mobile_request","KEY_F9");
                break;
        }
    }

}
