package com.crispcloudsolutions.rajmeditech;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    private AppCompatButton login;
    String password,dbpin="";
    private EditText et_password;
    private Window w;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        w=getWindow();
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        login = (AppCompatButton) findViewById(R.id.img_btn_login);
        Boolean password_pro_on = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Settings.KEY_PREF_PASSWORD_PRODECTION, Settings.DEFAULT_PREF_PASSWORD_PRODECTION);
        if (!password_pro_on) {

            startActivity(new Intent(getApplicationContext(), RemoteActivity.class));
            finish();
        } else {
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                        dbpin = app_preferences.getString("password", "1234");

                        et_password = (EditText) findViewById(R.id.et_password);
                        password = et_password.getText().toString();
                        if (password.equals(dbpin)) {
                            startActivity(new Intent(getApplicationContext(), RemoteActivity.class));
                            finish();
                        } else {
                            Animation shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                            et_password.setText("");
                            et_password.startAnimation(shake);
                            Toast.makeText(MainActivity.this, "Default Pin:1234", Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            w.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

}
