package com.crispcloudsolutions.rajmeditech;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.PictureDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.caverock.androidsvg.SVG;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.pixplicity.sharp.OnSvgElementListener;
import com.pixplicity.sharp.Sharp;
import com.pixplicity.sharp.SharpPicture;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import android.os.Handler;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class RemoteFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    ImageView bt_up,bt_left,bt_right,bt_down,bt_select;
    ImageButton bt_home,bt_l1,bt_l2;
    LinearLayout l1,l2,l3;
    ImageButton chg_buttonbutton;
    Socket socket;
    String url="http://192.168.42.1:2000/";
    String imageurl="";
    private Sharp mSvg;
    ImageView art,art_rgb1,art_rgb2;
    Bitmap bitmap;
    Bitmap O;
    InputStream instream,instream1;
    String urldisplay,urldisplay1="";
    View viewpar;
    Handler hos;

    Button  number0,
            number1,
            number2,
            number3,
            number4,
            number5,
            number6,
            number7,
            number8,
            number9,
            title,
            menu,
            reverse,
            display,
            subtitle,
            language,
            start,
            hash,
            clear,
            pause,
            forward;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_remote, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bt_up=(ImageView)view.findViewById(R.id.up);
        bt_left=(ImageView)view.findViewById(R.id.left);
        bt_right=(ImageView)view.findViewById(R.id.right);
        bt_down=(ImageView)view.findViewById(R.id.down);
		bt_select=(ImageView)view.findViewById(R.id.select);
        bt_home=(ImageButton)view.findViewById(R.id.bt_home);
        bt_l1=(ImageButton)view.findViewById(R.id.bt_language1);
        bt_l2=(ImageButton)view.findViewById(R.id.bt_language2);
        chg_buttonbutton=(ImageButton) view.findViewById(R.id.chg_buttonbutton);
        art=(ImageView) view.findViewById(R.id.art);
        art_rgb1=(ImageView) view.findViewById(R.id.art_rgb1);
        art_rgb2=(ImageView) view.findViewById(R.id.art_rgb2);
        l1=(LinearLayout) view.findViewById(R.id.l1);
        l2=(LinearLayout) view.findViewById(R.id.l2);
        l3=(LinearLayout) view.findViewById(R.id.l3);

        number0=(Button)getActivity().findViewById(R.id.number0);
        number1=(Button)getActivity().findViewById(R.id.number1);
        number2=(Button)getActivity().findViewById(R.id.number2);
        number3=(Button)getActivity().findViewById(R.id.number3);
        number4=(Button)getActivity().findViewById(R.id.number4);
        number5=(Button)getActivity().findViewById(R.id.number5);
        number6=(Button)getActivity().findViewById(R.id.number6);
        number7=(Button)getActivity().findViewById(R.id.number7);
        number8=(Button)getActivity().findViewById(R.id.number8);
        number9=(Button)getActivity().findViewById(R.id.number9);
        title=(Button)getActivity().findViewById(R.id.title);
        menu=(Button)getActivity().findViewById(R.id.menu);
        reverse=(Button)getActivity().findViewById(R.id.reverse);
        display=(Button)getActivity().findViewById(R.id.display);
        subtitle=(Button)getActivity().findViewById(R.id.subtitle);
        language=(Button)getActivity().findViewById(R.id.language);
        start=(Button)getActivity().findViewById(R.id.start);
        hash=(Button)getActivity().findViewById(R.id.hash);
        clear=(Button)getActivity().findViewById(R.id.clear);
        pause=(Button)getActivity().findViewById(R.id.pause);
        forward=(Button)getActivity().findViewById(R.id.forward);

        number0.setOnClickListener(this);
        number1.setOnClickListener(this);
        number2.setOnClickListener(this);
        number3.setOnClickListener(this);
        number4.setOnClickListener(this);
        number5.setOnClickListener(this);
        number6.setOnClickListener(this);
        number7.setOnClickListener(this);
        number8.setOnClickListener(this);
        number9.setOnClickListener(this);
        title.setOnClickListener(this);
        menu.setOnClickListener(this);
        reverse.setOnClickListener(this);
        display.setOnClickListener(this);
        subtitle.setOnClickListener(this);
        language.setOnClickListener(this);
        start.setOnClickListener(this);
        hash.setOnClickListener(this);
        clear.setOnClickListener(this);
        pause.setOnClickListener(this);
        forward.setOnClickListener(this);



        //viewpar=view;

        Log.d("gggggg","hhhhhhhhhhh");

//        Sharp.loadResource(getResources(), R.raw.cartman)
//                .into(art);

    // new DownloadSVGTask(art).execute("https://cdn.shopify.com/s/files/1/0496/1029/files/Freesample.svg?5153");

        try {
            socket = IO.socket("http://192.168.42.1:3000");
            hos=new Handler();

        }catch (Exception e){
            e.printStackTrace();
        }
        bt_up.setOnClickListener(this);
        bt_left.setOnClickListener(this);
        bt_right.setOnClickListener(this);
        bt_down.setOnClickListener(this);
		bt_select.setOnClickListener(this);
        bt_home.setOnClickListener(this);
        bt_l1.setOnClickListener(this);
        bt_l2.setOnClickListener(this);
        chg_buttonbutton.setOnClickListener(this);

        //new HttpImageRequestTask().execute();
        //new DownloadSVGTask(art).execute("http://upload.wikimedia.org/wikipedia/commons/e/e8/Svg_example3.svg");

        socket.connect();
        socket.on("slide_image", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                Log.d("ggggggggg",args[0].toString());

                if (args[0].toString().contains(".svg")) {
                    new DownloadSVGTask(art).execute(url + args[0].toString());
                } else {
                    new DownloadImageTask(art)
                            .execute(url + args[0].toString());
                }



            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            socket = IO.socket("http://192.168.42.1:3000");
            hos=new Handler();
            socket.connect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            if (socket.connected())
            socket.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            urldisplay = urls[0];
            Bitmap mIcon11 = null;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(urldisplay).build();
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {

            }
            mIcon11=BitmapFactory.decodeStream(response.body().byteStream());


        }catch (Exception e){
            Log.e("gggggg",e.toString());
        }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private class DownloadSVGTask extends AsyncTask<String, Void, String> {
        ImageView bmImage;
        InputStream instr=null;

        public DownloadSVGTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected String doInBackground(String... urls) {
            String urldisplay = urls[0];
            urldisplay1 = urls[0];
            try {
                OkHttpClient client = new OkHttpClient();


                Log.e("urldisplay   ",urldisplay );



                Response response,response1 ;

                if(urldisplay1.contains("/Rgb")) {
                    Request request1 = new Request.Builder().url(urldisplay).build();
                    response1 = client.newCall(request1).execute();
                    instream1=response1.body().byteStream();
                    Request request = new Request.Builder().url(urldisplay).build();
                    response = client.newCall(request).execute();
                    instream=response.body().byteStream();
                    Sharp.loadInputStream(instream).into(art_rgb1);
                    Sharp.loadInputStream(instream1).into(art_rgb2);
                }else {
                    Request request = new Request.Builder().url(urldisplay).build();
                    response = client.newCall(request).execute();
                    instream=response.body().byteStream();
                    Sharp.loadInputStream(instream).into(art);
                }
//                Sharp.loadResource(getResources(), R.raw.cartman).into(art_rgb1);
//                Sharp.loadResource(getResources(), R.raw.cartman).into(art_rgb2);
//                Sharp.loadResource(getResources(), R.raw.cartman).into(art);


            } catch (Exception e) {
                Log.e("gggggg", e.getMessage(), e);
            }
            return null;
        }
        protected void onPostExecute(String s) {

            l3.setVisibility(View.GONE);
            art.setVisibility(View.INVISIBLE);
            l1.setVisibility(View.GONE);
            l2.setVisibility(View.GONE);

            if(urldisplay1.contains("/Rgb")){
                l1.setVisibility(View.VISIBLE);
                l2.setVisibility(View.VISIBLE);
                l3.setVisibility(View.VISIBLE);
            }else{
                art.setVisibility(View.VISIBLE);
            }


        }
    }

//    private void loadNet(AppCompatImageView bmImage,String url) {
//        Glide.with(getActivity())
//                .load(url)
//                .placeholder(R.drawable.image_loading)
//                .error(R.drawable.image_error)
//                .into(bmImage);
//    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.up:
                socket.emit("mobile_request","KEY_UP");
                break;
            case R.id.left:
                socket.emit("mobile_request","KEY_LEFT");
                break;
            case R.id.right:
                socket.emit("mobile_request","KEY_RIGHT");
                break;
            case R.id.down:
                socket.emit("mobile_request","KEY_DOWN");
                break;
			case R.id.select:
				socket.emit("mobile_request","KEY_ENTER");
				break;
            case R.id.bt_home:
                socket.emit("mobile_request","KEY_GREEN");
                break;
            case R.id.bt_language1:
                socket.emit("mobile_request","KEY_1");
                break;
            case R.id.bt_language2:
                socket.emit("mobile_request","KEY_2");
                break;
            case R.id.chg_buttonbutton:
                socket.emit("mobile_request","KEY_MUTE");
                break;
            case R.id.number0:
                socket.emit("mobile_request","KEY_MENU");
                break;
            case R.id.number1:
                socket.emit("mobile_request","KEY_RED");
                break;
            case R.id.number2:
                socket.emit("mobile_request","KEY_YELLOW");
                break;
            case R.id.number3:
                socket.emit("mobile_request","KEY_3");
                break;
            case R.id.number4:
                socket.emit("mobile_request","KEY_4");
                break;
            case R.id.number5:
                socket.emit("mobile_request","KEY_5");
                break;
            case R.id.number6:
                socket.emit("mobile_request","KEY_6");
                break;
            case R.id.number7:
                socket.emit("mobile_request","KEY_7");
                break;
            case R.id.number8:
                socket.emit("mobile_request","KEY_8");
                break;
            case R.id.number9:
                socket.emit("mobile_request","KEY_9");
                break;
            case R.id.title:
                socket.emit("mobile_request","KEY_E");
                break;
            case R.id.menu:
                socket.emit("mobile_request","KEY_0");
                break;
            case R.id.reverse:
                socket.emit("mobile_request","KEY_F1");
                break;
            case R.id.display:
                socket.emit("mobile_request","KEY_F2");
                break;
            case R.id.subtitle:
                socket.emit("mobile_request","KEY_F3");
                break;
            case R.id.language:
                socket.emit("mobile_request","KEY_F4");
                break;
            case R.id.start:
                socket.emit("mobile_request","KEY_F5");
                break;
            case R.id.hash:
                socket.emit("mobile_request","KEY_F6");
                break;
            case R.id.clear:
                socket.emit("mobile_request","KEY_F7");
                break;
            case R.id.pause:
                socket.emit("mobile_request","KEY_F8");
                break;
            case R.id.forward:
                socket.emit("mobile_request","KEY_F9");
                break;

        }
    }
    public void requestRead() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {

        }
    }
    private void change_colorimage(Context ctx) {

        l1.setBackgroundColor(Color.parseColor("#FF0000"));
        l2.setBackgroundColor(Color.parseColor("#008000"));
        Toast.makeText(getActivity(),"changed ",Toast.LENGTH_LONG).show();
//        ShapeDrawable shapeDrawable = new ShapeDrawable();
//
//       BitmapDrawable BD = (BitmapDrawable) art.getDrawable();
//        bitmap = BD.getBitmap();
//        O = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), bitmap.getConfig());

      //  lLayout.setBackgroundColor(Color.parseColor("#000000"));
//
//        for(int i=0; i<bitmap.getWidth(); i++){
//            for(int j=0; j<bitmap.getHeight(); j++){
//                int p = bitmap.getPixel(i, j);
//                int b = Color.blue(p);
//
//                int x =  0;
//                int y =  0;
//                b =  b+150;
//                O.setPixel(i, j, Color.argb(Color.alpha(p), x, y, b));
//            }
//        }
//        art.setImageBitmap(O);
   }




}
