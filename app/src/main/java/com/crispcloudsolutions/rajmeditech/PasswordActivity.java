package com.crispcloudsolutions.rajmeditech;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PasswordActivity extends AppCompatActivity {
    private Button changepin;
    private EditText et_oldpass,et_newpass1,et_newpass2;
    private TextView tv_oldpass,tv_newpass1,tv_newpass2;
    String oldpin,newpass1,newpass2,dboldpin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setContentView(R.layout.activity_password);

        et_newpass1=(EditText)findViewById(R.id.et_newpass1);
        et_newpass2=(EditText)findViewById(R.id.et_newpass2);
        et_oldpass=(EditText)findViewById(R.id.et_oldpass);
        changepin=(Button)findViewById(R.id.bt_change_pin);
        changepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldpin=et_oldpass.getText().toString().trim();
                newpass1=et_newpass1.getText().toString().trim();
                newpass2=et_newpass2.getText().toString().trim();
                try {
                    SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(PasswordActivity.this);

                    SharedPreferences.Editor editor = app_preferences.edit();
                    dboldpin=app_preferences.getString("password","1234");


                    if(oldpin.equals(dboldpin)){
                        if(newpass1.equals(newpass2)){
                            editor.putString("password",newpass1);
                            editor.commit();
                            Toast.makeText(PasswordActivity.this, "success...!", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                        else{
                            Toast.makeText(PasswordActivity.this, "Incorrect pin match", Toast.LENGTH_SHORT).show();
                            et_oldpass.setText("");
                            et_newpass1.setText("");
                            et_newpass2.setText("");
                            newpass1="";
                            newpass2="";
                            dboldpin="";
                            oldpin="";
                        }
                    }
                    else {
                        Animation shake= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.shake);
                        Toast.makeText(PasswordActivity.this, "Incorrect old pin"+dboldpin, Toast.LENGTH_SHORT).show();
                        et_oldpass.setText("");
                        et_newpass1.setText("");
                        et_newpass2.setText("");
                        et_oldpass.startAnimation(shake);
                        newpass1="";
                        newpass2="";
                        dboldpin="";
                        oldpin="";
                    }

                }
                catch (Exception e){
                    Toast.makeText(PasswordActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
