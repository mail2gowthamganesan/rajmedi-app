package com.crispcloudsolutions.rajmeditech;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

public class RemoteActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Socket socket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActivityCompat.requestPermissions(RemoteActivity.this,new String[]{Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.CHANGE_NETWORK_STATE,Manifest.permission.CHANGE_WIFI_STATE},4);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        android.support.v4.app.Fragment fragment=new RemoteFragment();
        if (fragment!=null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.framlayout, fragment).commit();
        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        try {
            socket = IO.socket("http://192.168.42.1:3000");
        }catch (Exception e){
            e.printStackTrace();
        }
        socket.connect();
        int id = item.getItemId();

        if (id == R.id.nav_remote) {
            android.support.v4.app.Fragment fragment=new RemoteFragment();
            if (fragment!=null){
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.framlayout, fragment).commit();
            }
        }else if (id == R.id.nav_numberpad){
            android.support.v4.app.Fragment fragment=new NumberPadFragment();
            if (fragment!=null){
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.framlayout, fragment).commit();
            }
        }
        else if (id == R.id.nav_distance) {
            socket.emit("mobile_request","KEY_MUTE");
            if (socket.connected()){
                socket.close();
                socket.disconnect();
            }
        } else if (id == R.id.nav_language1) {
            socket.emit("mobile_request","KEY_F10");
            if (socket.connected()){
                socket.close();
                socket.disconnect();
            }
        } else if (id == R.id.nav_language2) {
            socket.emit("mobile_request","KEY_F11");
            if (socket.connected()){
                socket.close();
                socket.disconnect();
            }
        }else if(id == R.id.nav_password) {
            Intent i=new Intent(getApplicationContext(),PasswordActivity.class);
            startActivity(i);

        }  else if (id == R.id.nav_settings) {
            Intent i=new Intent(getApplicationContext(),SettingsActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


}
